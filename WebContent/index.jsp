<!DOCTYPE html> 
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Properties" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="javax.naming.Context" %>
<%@page import="javax.naming.InitialContext" %>
<%@page import="javax.sql.DataSource" %>
<%@page import="gt.com.claro.pisa.npr.Menu" %>
<%@page import="gt.com.claro.pisa.npr.ugp.Users" %>
<%@page import="gt.com.claro.pisa.npr.ugp.Options" %>
<%
Properties Props = new Properties();
String propertyFileName = "conf.properties";
Props.load(getClass().getClassLoader().getResourceAsStream(propertyFileName));
String User = (request.getRemoteUser()!=null ? request.getRemoteUser() : Props.getProperty("user").toUpperCase());
final String aName = "NPR";

Options opcion = new Options(Props);
int alvl = opcion.getAppLvl(aName);		// App's Security level

Menu myMenu = new Menu();
Users user = new Users(Props, User);
String UName = user.getLocalNombre();

%>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>New Pisa Reports</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<link rel="shortcut icon" href="images/favicon.ico">
	<link href="css/bootstrap.css" rel="stylesheet" media="screen">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
	<link href="css/kreaker.css" rel="stylesheet">
</head>
<body>
<%
if ( user.getLocalSecLvl()>=alvl ) {
%>
	<div class="navbar navbar-default navbar-fixed-top"><!-- Fixed navbar -->
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.jsp" title="Reportes GAT">NPR</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<%=myMenu.getMenuTreeBS(myMenu.getUserMenu(User),true)%>
					<li id="fat-menu">
						<a href="logout.jsp" id="logout" role="button">Salir</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				<li><a href="#" title="<%=UName%>"><span class="label label-primary"><%=User%></span></a></li>
				</ul>
			</div>
		</div>
	</div> <!-- Fixed navbar End-->
<div class="container">
	<div class="page-header">
		<h2>New Pisa Reports</h2>
	</div>
</div>
<!-- Footer -->
<footer class="footer">
	<div class="col-md-12 text-left"><span class="label label-default">Copyright &copy; 2015 Alejandro Lopez Monzon</span></div>
</footer>
<%
}else{
%>
<body>
	<div class="container">
	    <div class="alert alert-danger">
			<h2>Error!</h2>
			<h4>El usuario <%=User%> no tiene acceso a esta aplicacion</h4>
		</div>
	</div>
<%
}
%>
<!-- Script loading... -->
<script src="js/jquery.js"></script>
<script src="js/dropdown.js"></script>
<script src="js/kreaker.js"></script>
</body>
</html>
