package org.alexlm78.utils.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
* Query Executer by Kreaker
* 
* @author Alejandro Lopez Monzon (Kreaker)
* @version 1.0
* @category SQL.
*/
public class KQueryExec 
{
	private Connection dbConn=null;
	private Properties Props;
	private Boolean DEBUG = false;
	
	public KQueryExec( Properties prop )
	{
		Props = prop;
		if ( prop != null )
		{
			this.Connect();
			if ( Props.getProperty("debug")!=null )
				DEBUG = ( Props.getProperty("debug").trim().compareToIgnoreCase("true") == 0) ? true : false;
		}
	}
	
	public void Connect()
	{		
		try 
		{
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			dbConn = ds.getConnection();
			if(DEBUG) System.out.println(dbConn.toString());
		}catch ( Exception ex )
		{
			if (DEBUG) System.err.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	public String createFile( String query, String arch )
	{
		if (DEBUG) System.out.println("Archivo a generar: " + arch);
		String Res = "";
		
		try
		{
			String sRuta = Props.getProperty("path");
			String sArch = sRuta+arch.trim();
			
			java.io.Writer wSalida;
			
			wSalida = new java.io.BufferedWriter(new java.io.FileWriter(sArch));
			
			Statement st = dbConn.createStatement();			
			if (DEBUG) System.out.println(query);
			ResultSet rs = st.executeQuery(query);
							
			int cols = rs.getMetaData().getColumnCount();
										
			for ( int i=1; i<=cols; i++ )
				wSalida.write("\""+rs.getMetaData().getColumnLabel(i).trim()+"\",");
				
			wSalida.write(System.getProperty("line.separator"));
			
			
			while ( rs.next() )
			{  
				//rs.getO
				for ( int i=1; i<=cols; i++ )   
					wSalida.write( (rs.getObject(i)==null) ? "\"\"," : "\""+rs.getObject(i).toString().trim()+"\",");
										
				wSalida.write(System.getProperty("line.separator"));
			}
			
			rs.close();
			st.close();
			wSalida.close();
			Res = sArch;
		}catch( Exception ex )
		{
			System.err.println(ex.getMessage());			
			Res="";
		}
		
		return Res;		
	}
	
	public String createTextFile( String query, String arch )
	{
		if (DEBUG) System.out.println("Archivo a generar: " + arch);
		String Res = "";
		
		try
		{
			String sRuta = Props.getProperty("path");
			String sArch = sRuta+arch.trim();
			
			java.io.Writer wSalida;
			
			wSalida = new java.io.BufferedWriter(new java.io.FileWriter(sArch));
			
			Statement st = dbConn.createStatement();			
			if (DEBUG) System.out.println(query);
			ResultSet rs = st.executeQuery(query);
							
			while ( rs.next() )
			{  
				//rs.getO
				wSalida.write( (rs.getObject(1)==null) ? "\"\"" : rs.getObject(1).toString().trim());
				wSalida.write(System.getProperty("line.separator"));
			}
			
			rs.close();
			st.close();
			wSalida.close();
			Res = sArch;
		}catch( Exception ex )
		{
			System.err.println(ex.getMessage());
			Res="";
		}
		
		return Res;		
	}
	
	public Integer executeInsert( String query )
	{
		Integer Res = -1;
		
		try
		{
			Statement st = dbConn.createStatement();
			if (DEBUG) System.out.println(query);
			Res = st.executeUpdate(query);
			st.close();
			
		}catch( Exception ex )
		{
			System.err.println(ex.getMessage());
		}
		
		return Res;		
	}
	
	public ArrayList<Integer> executeInsert( ArrayList<String> querys )
	{
		ArrayList<Integer> Res = new ArrayList<Integer>();
		
		try
		{
			Statement st = dbConn.createStatement();
			for( int i=0; i<querys.size(); i++)
			{
				if (DEBUG) System.out.println(querys.get(i));
				Res.add(st.executeUpdate(querys.get(i)));
			}
			st.close();
			
		}catch( Exception ex )
		{
			System.err.println(ex.getMessage());
		}
		
		return Res;
	}
	
	public Integer executeUpdate(String query)
	{
		return 1;
	}
	
	public ArrayList<Integer> executeUpdate( ArrayList<String> querys )
	{
		ArrayList<Integer> Res = new ArrayList<Integer>();
		Res.add(1);
		return Res;
	}
	
	public ResultSet executeSelect ( String query )
	{
		ResultSet Res = null;
		
		try
		{
			Statement st = dbConn.createStatement();
			
			if (DEBUG) System.out.println(query);
			
			Res = st.executeQuery(query);
			return Res;
		}catch( Exception ex )
		{
			System.err.println(ex.getMessage());
		}
		
		return null;
	}
	
	public ArrayList<ResultSet> executeSelect ( ArrayList<String> querys )
	{
		ArrayList<ResultSet> rs = new ArrayList<ResultSet>();
		rs.add(null);
		return rs;
	}
	
	public Boolean Existe( String table )
	{
		Boolean Res = false;
		
		String schema = table.substring(0, table.indexOf("."));
		String tabla = table.substring(table.indexOf(".")+1, table.length());
		
		String query = "SELECT * FROM SYSIBM.SQLTABLES WHERE TABLE_SCHEM='"+schema.toUpperCase()+"' AND TABLE_NAME='"+tabla.toUpperCase()+"' AND TABLE_TYPE='TABLE'";
		if (DEBUG) System.out.println(query);
		
		try
		{
			Statement st = dbConn.createStatement();			
			ResultSet rs = st.executeQuery(query);
			
			if ( rs.next() )
			{
				Res = true;
				if (DEBUG) System.out.println("Archivo "+ table + " existe");
			}
			
			rs.close();
			st.close();			
		}catch( Exception ex )
		{
			//log.error(ex.getMessage());
			System.err.println(ex.getMessage());
			Res=false;
		}
		
		return Res;
	}
	
	public Boolean DropT( String table )
	{
		Boolean Res = false;
		
		String schema = table.substring(0, table.indexOf("."));
		String tabla = table.substring(table.indexOf(".")+1, table.length());
		
		String query = "DROP TABLE "+schema+"."+tabla;
		if (DEBUG) System.out.println(query);
		
		try
		{
			Statement st = dbConn.createStatement();			
			Integer rs = st.executeUpdate(query);
			
			if ( rs>=1 ) 
				Res=true;
			if (DEBUG) System.out.println("Archivo "+ table + " eliminado");

			st.close();			
		}catch( Exception ex )
		{
			//log.error(ex.getMessage());
			System.err.println(ex.getMessage());
			Res=false;
		}
		
		return Res;
	}
}
