package gt.com.claro.pisa.npr;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Constructor querys over NPR object.
 * 
 * @author Alejandro
 * @category Reports
 */
public class ReportQuery 
{
	private String sReport="";
	private Properties Props;
	private Connection dbConn;
	
	/**
	 * Contructor 
	 * 
	 * @param prop Porperties for DB connection.
	 */
	public ReportQuery( Properties prop )
	{
		Props = prop;
	}
	
	/**
	 * Contructor 
	 * 
	 * @param prop Properties for DB Connections
	 * @param report New report name to get information.
	 * @throws Throwable Error hanldlers.
	 */
	public ReportQuery( Properties prop, String report ) throws Throwable
	{
		Props = prop;
		setReport(report);
	}
	
	/**
	 * Connect a specified DataSource.
	 *  
	 * @return Success or fail.
	 * @throws SQLException SQLError to connect DB
	 * @throws NamingException Context error.
	 */
	private boolean Connect() throws SQLException, NamingException
	{
		Context ctx = new InitialContext();
	
		DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
		dbConn = ds.getConnection();
		
		return dbConn!=null?true:false;
	}
	
	/**
	 * Set a new report for this object.
	 * 
	 * @param report New report name to get information.
	 * @throws Exception Errors handled
	 */
	public void setReport( String report ) throws Exception
	{
		String temp = sReport;
		sReport = report;
		if ( !validateReport() )
		{
			sReport = temp;
			System.out.println("Report "+sReport+" is not a valid or existent report!!!");
		}
	}
	
	/**
	 * Validate if the setted report is a valid report.
	 * 
	 * @return true if the actual report is valid.
	 * @throws Exception Some error can happend, this handle it.
	 */
	public boolean validateReport() throws Exception
	{
		boolean bRes = false;
		
		if ( dbConn==null ) this.Connect();
		
		Statement st = dbConn.createStatement();
		ResultSet rs = st.executeQuery("SELECT REPCOD,REPDESC,JNDI,IFACE,SEGSTS FROM PALMOBJPGM.NPR_RINFO WHERE REPCOD='"+sReport+"' ");
		
		bRes = rs.next() ? true : false;
		
		rs.close();
		st.close();
		
		return bRes;
	}
}
