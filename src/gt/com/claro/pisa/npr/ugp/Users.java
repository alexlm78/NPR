package gt.com.claro.pisa.npr.ugp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.ArrayList;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Users
{
	private String User=null;
	private String Nombre="";
	private int Group=-1;
	private int SecLvl=-1;
	private Connection dbConn=null;
	private Properties Props;
	private Boolean DEBUG = false;
	
	
	public Users( Properties prop, String user )
	{
		User = (user.trim().length()>0) ? user.trim().toUpperCase() : null ;
		if ( prop != null )
		{
			Props = prop;
			this.Connect();
			if ( Props.getProperty("debug")!=null )
				DEBUG = ( Props.getProperty("debug").compareToIgnoreCase("true") == 0) ? true : false;
			
			this.getLocalInfo();
		}
	}
	
	/**
	 * Get the Name and secutiry level of current local user.
	 */
	private void getLocalInfo()
	{
		try
		{
			String query = "SELECT NOMBRE,IDGRUPO,STSSEG FROM PALMOBJPGM.A_USERS WHERE LOGIN='"+User+"'";
			Statement st = dbConn.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			if ( rs.next() )
			{
				Nombre = rs.getString("NOMBRE").trim();
				Group = rs.getInt("IDGRUPO");
				SecLvl = rs.getInt("STSSEG");
				
			}
		}catch( Exception ex )
		{
			ex.printStackTrace();
		}
		
	}
	
	/**
	 * Make an active conection for a class life time
	 */
	private void Connect()
	{		
		try 
		{
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			dbConn = ds.getConnection();
			if(DEBUG) System.out.println(dbConn.toString());
		}catch ( Exception ex )
		{
			if (DEBUG) System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * Get the full name of the current local user.
	 * @return
	 */
	public String getLocalNombre() {
		return Nombre;
	}
	
	/**
	 * Get the security level of current local user.
	 * 
	 * @return int security level
	 */
	public int getLocalSecLvl() {
		return SecLvl;
	}

	/**
	 * Get the User login of the current local user.
	 * @return String User login
	 */
	public String getLocalUser() {
		return User;
	}
	
	/**
	 * Get the idGroup of the current local user.
	 * @return idgroup for User.
	 */
	public int getLocalGroup(){
		return Group;
	}
	
	/**
	 * Get the information about an id of a user.
	 * 
	 * @param idUser User identification number.
	 * @return Array with user informacion. 
	 */
	public ArrayList<String> getUserInfo( String idUser )
	{
		ArrayList<String> tmp = new ArrayList<String>();
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT IDUSER,LOGIN,NOMBRE,EMAIL,IDGRUPO,STSSEG FROM PALMOBJPGM.A_USERS S WHERE IDUSER="+idUser;
			ResultSet rs = st.executeQuery(query);
			
			
			if ( rs.next() )
			{
				for ( int i=1; i<=6; i++ )
					tmp.add(rs.getString(i).trim());
			}
			
			rs.close();
			st.close();			
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
	
	/**
	 * Get the number of users on system.
	 * 
	 * @return the total numbre of users.
	 */
	public int getTotalUsuarios()
	{
		int tmp = 0;
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT COUNT(*) FROM PALMOBJPGM.A_USERS";
			ResultSet rs = st.executeQuery(query);
						
			if ( rs.next() )
				tmp = rs.getInt(1);
			
			rs.close();
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
	
	/**
	 * Actualiza datos de un usuario.
	 * 
	 * @param idUser
	 * @param login
	 * @param nombre
	 * @param email
	 * @param grupo
	 * @param seg
	 * @return Exito o fracaso.
	 */
	public boolean updateUser( String idUser, String login, String nombre, String email, Integer grupo, Integer seg)
	{
		String query="";
		int res=-1;
		
		try 
		{
			Statement st = dbConn.createStatement();			
			query = "UPDATE PALMOBJPGM.A_USERS SET "+
					"NOMBRE='"+nombre+"',"+
					"EMAIL='"+email+"',"+
					"IDGRUPO="+grupo+","+
					"STSSEG="+seg+" "+
					"WHERE IDUSER="+idUser+" AND LOGIN='"+login+"'";
			res = st.executeUpdate(query);
						
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return (res>0)?true:false;
	}
	
	public boolean createUser( String login, String nombre, String email, Integer grupo, Integer seg)
	{
		String query="";
		int res=-1;
		
		try 
		{
			Statement st = dbConn.createStatement();			
			query = "INSERT INTO PALMOBJPGM.A_USERS "+
					"(LOGIN,NOMBRE,EMAIL,IDGRUPO,USUARIO,STSSEG) VALUES "+
					"('"+login+"','"+nombre+"','"+email+"',"+grupo+",'"+this.getLocalUser()+"',"+seg+")";
			
			res = st.executeUpdate(query);
						
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return (res>0)?true:false;
	}
	
	/**
	 * Actualiza la pass de un usuario
	 * @param iduser
	 * @param login
	 * @param pass
	 * @return
	 */
	public boolean updatePass( String login, String pass)
	{
		//public int chgPwdHttp(String user, String pass) {
			
		String cmd = "";
		boolean fallo = false;
		
		try 
		{
			//cmd = new StringBuffer(rutaComando).append("htpasswd -mb ").append(rutaArchivo).append(" ").append(user).append(" ").append(pass).toString();
			cmd = "/opt/IBM/HTTPServer/bin/htpasswd -mb /opt/IBM/HTTPServer/conf/ArchPasswords "+login+" "+pass+"";
				
			Process p = Runtime.getRuntime().exec(cmd);
			
			int exitCode = p.waitFor();
			
			fallo = (exitCode == 0) ? true : false;
		
		}catch (IOException e) 
		{
			//logger.error(new StringBuffer("_No se pudo ejecutar el comando '").append(cmd).append("': ").append(e).toString());
			System.out.println(e.getMessage());
			e.printStackTrace();
		}catch (InterruptedException ie) 
		{
			//logger.error("_Proceso interrumpido: " + ie);
			System.out.println(ie.getMessage());
			ie.printStackTrace();
		}
		
		return fallo;
	}
}
