package gt.com.claro.pisa.npr.ugp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Options 
{
	private Connection dbConn=null;
	private Properties Props;
	private Boolean DEBUG = true;
	
	/**
	 * Contructor.
	 * 
	 * @param prop conection data.
	 */
	public Options(Properties prop)
	{
		if ( prop != null )
		{
			Props = prop;
			this.Connect();
			if ( Props.getProperty("debug")!=null )
				DEBUG = ( Props.getProperty("debug").compareToIgnoreCase("true") == 0) ? true : false;
		}
	}
	
	/**
	 * Make an active conection for a class life time
	 */
	private void Connect()
	{		
		try 
		{
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			dbConn = ds.getConnection();
			if(DEBUG) System.out.println(dbConn.toString());
		}catch ( Exception ex )
		{
			if (DEBUG) System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	/**
	 * Get the count of opciont type 'P' (Programs)
	 * @return
	 */
	public int getTotalProgramas()
	{
		int tmp = 0;
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT COUNT(*) FROM PALMOBJPGM.A_OPTIONS WHERE TIPO='P'";
			ResultSet rs = st.executeQuery(query);
						
			if ( rs.next() )
				tmp = rs.getInt(1);
			
			rs.close();
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
	
	/**
	 * Get the count of options type 'M' (Menus)
	 * @return
	 */
	public int getTotalMenus()
	{
		int tmp = 0;
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT COUNT(*) FROM PALMOBJPGM.A_OPTIONS WHERE TIPO='M'";
			ResultSet rs = st.executeQuery(query);
						
			if ( rs.next() )
				tmp = rs.getInt(1);
			
			rs.close();
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
	
	/**
	 * Get the full count of options
	 * @return
	 */
	public int getTotal()
	{
		int tmp = 0;
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT COUNT(*) FROM PALMOBJPGM.A_OPTIONS";
			ResultSet rs = st.executeQuery(query);
						
			if ( rs.next() )
				tmp = rs.getInt(1);
			
			rs.close();
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
	
	/**
	 * Get the Secutiry level for a specific app.
	 * 
	 * @param opcion Specific App-
	 * @return Given App's security level
	 */
	public int getAppLvl( String opcion )
	{
		int tmp = -1;
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT STSSEG FROM PALMOBJPGM.A_OPTIONS WHERE OPCION='"+opcion.toUpperCase().trim()+"'";
			ResultSet rs = st.executeQuery(query);
						
			if ( rs.next() )
				tmp = rs.getInt(1);
			
			rs.close();
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
				
		return (tmp<0)?999:tmp;
	}
}
