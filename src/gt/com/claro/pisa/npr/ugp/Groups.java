package gt.com.claro.pisa.npr.ugp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Groups 
{
	private Connection dbConn=null;
	private Properties Props;
	private Boolean DEBUG = true;
	
	/**
	 * Contructor.
	 * 
	 * @param prop conection data.
	 */
	public Groups(Properties prop)
	{
		if ( prop != null )
		{
			Props = prop;
			this.Connect();
			if ( Props.getProperty("debug")!=null )
				DEBUG = ( Props.getProperty("debug").compareToIgnoreCase("true") == 0) ? true : false;
		}
	}
	
	/**
	 * Make an active conection for a class life time
	 */
	private void Connect()
	{		
		try 
		{
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			dbConn = ds.getConnection();
			if(DEBUG) System.out.println(dbConn.toString());
		}catch ( Exception ex )
		{
			if (DEBUG) System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	public int getTotalGrupos()
	{
		int tmp = 0;
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT COUNT(*) FROM PALMOBJPGM.A_GROUPS";
			ResultSet rs = st.executeQuery(query);
						
			if ( rs.next() )
				tmp = rs.getInt(1);
			
			rs.close();
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
	
	public boolean createGroup( String nombre, String menui, String user, Integer seg)
	{
		String query="";
		int res=-1;
		
		try 
		{
			Statement st = dbConn.createStatement();			
			query = "INSERT INTO PALMOBJPGM.A_GROUPS "+
					"(NOMBRE,MENUINI,USUARIO,STSSEG) VALUES "+
					"('"+nombre+"','"+menui+"','"+user+"',"+seg+")";
			
			res = st.executeUpdate(query);
						
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return (res>0)?true:false;
	}
	
	public boolean updateGroup( String idGrupo, String nombre, String menui, Integer seg)
	{
		String query="";
		int res=-1;
		
		try 
		{
			Statement st = dbConn.createStatement();			
			query = "UPDATE PALMOBJPGM.A_GROUPS SET "+
					"NOMBRE='"+nombre+"',"+
					"MENUINI='"+menui+"',"+
					"STSSEG="+seg+" "+
					"WHERE IDGRUPO="+idGrupo;
			res = st.executeUpdate(query);
						
			st.close();
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return (res>0)?true:false;
	}
	
	public ArrayList<String> getGroupInfo( String idGroup )
	{
		ArrayList<String> tmp = new ArrayList<String>();
		try 
		{
			Statement st = dbConn.createStatement();			
			String query = "SELECT IDGRUPO,NOMBRE,MENUINI,STSSEG FROM PALMOBJPGM.A_GROUPS WHERE IDGRUPO="+idGroup.toUpperCase().trim();
			ResultSet rs = st.executeQuery(query);
			
			
			if ( rs.next() )
			{
				for ( int i=1; i<=4; i++ )
					tmp.add(rs.getString(i).trim());
			}
			
			rs.close();
			st.close();			
		}catch ( Exception ex )
		 {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		return tmp;
	}
}
