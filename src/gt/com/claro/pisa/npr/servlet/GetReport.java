package gt.com.claro.pisa.npr.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import gt.com.claro.pisa.npr.Menu;

public class GetReport extends Menu 
{
	private static final long serialVersionUID = 562796645624304898L;
	private Connection dbConn=null;

	/**
	 * Constructor of the object.
	 */
	public GetReport() {
		super();
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 * @throws  
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		super.doGet(request, response);
		String sTitle = "New Pisa Reports";
		String jndi = "";
		Integer iFace = 0;
		boolean bNoRepErr = false;
		boolean bNoAuthErr = false;
		Integer iRepSecLvl = 0;
		Integer uSecLvl = getUserSecLvl(User);
		
		String sReporte = ( request.getParameter("r")!=null ) ? request.getParameter("r").toString().trim() : "";
		
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//DateFormat sdfi = new SimpleDateFormat("yyyyMMdd");
		Date hoy = Calendar.getInstance().getTime();
		long tiempoActual = hoy.getTime();
		long unDia = 1 * 86400000; 				//(24 * 60 * 60 * 1000)=86400000
		Date ayer = new Date(tiempoActual - unDia);
		
		String sAyer = sdf.format(ayer);
		String sHoy = sdf.format(hoy);
		//String iAyer = sdfi.format(ayer);
		//String iHoy = sdfi.format(hoy);
		
		try
		{
			Properties prop = new Properties();
			String propertyFileName = this.getInitParameter("myConf");
			prop.load(getClass().getClassLoader().getResourceAsStream(propertyFileName));
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			dbConn = ds.getConnection();
			
			//out.println("Conn: "+dbConn.toString());
			Statement st = dbConn.createStatement();
			ResultSet rs = st.executeQuery("SELECT REPCOD,REPDESC,JNDI,IFACE,SEGSTS FROM PALMOBJPGM.NPR_RINFO WHERE REPCOD='"+sReporte+"' ");
			
			if ( rs.next() )
			{
				sTitle = rs.getString(2).trim();
				jndi = rs.getString(3).trim();
				iFace = rs.getInt(4);
				iRepSecLvl = rs.getInt(5);
				bNoAuthErr = ( uSecLvl >= iRepSecLvl ) ? false:true;
			}
			else
				bNoRepErr = true;
			
			rs.close();
		}catch (Exception sex )
		{
			sex.printStackTrace();
		}
		
		out.println("<div class=\"page-header\">");
		out.println("<H1>"+sTitle+"</H1></div>");
		
		if ( bNoRepErr )
			out.println("<div class=\"col-md-5 col-md-offset-3 alert alert-warning\"><h4>"+sReporte+" no existe como reporte, favor validar</h4></div>");
		else if ( bNoAuthErr )
			out.println("<div class=\"col-md-5 col-md-offset-3 alert alert-danger\"><h3>"+User+" no tiene acceso a este reporte!!</h3></div>");
		else
		{
			//out.println("<center>");
			out.println("<form method=\"post\">");
			out.println("	<div class=\"form-group\">");
			//out.println("	 <div class=\"input-prepend input-group col-md-5 col-md-offset-3\">");
			out.println("		<span class=\"add-on input-group-addon\"><i class=\"glyphicon glyphicon-calendar fa fa-calendar\"></i></span>");
			out.println("		<input type=\"text\" style=\"width: 200px\" name=\"fecha\" id=\"fecha\" class=\"form-control\" value=\""+sAyer+" - "+sHoy+"\" />");
			out.println("	</div>");
			//out.println("	 <br/>");
			out.println("	<div>");
			out.println("		<button type=\"submit\" class=\"btn btn-default\">Procesar</button>");
			out.println("	</div>");
			//out.println("	</div>");
			//out.println("  </div>");
			out.println("<input type=\"hidden\" id=\"r\" name=\"r\" value=\""+sReporte+"\"");
			out.println("</form>");
			//out.println("</center>");
		}
		
		System.out.println("T:"+sTitle+" j:"+jndi+" i:"+iFace.toString()+" auth:"+iRepSecLvl+" Ul:"+uSecLvl);
		out.println(this.getFooterBS());
		out.println("</body>\n</html>\n");
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		super.doPost(request, response);
		
		String sReporte = ( request.getParameter("r")!=null ) ? request.getParameter("r").toString().trim() : "";
		
		try
		{
			Properties prop = new Properties();
			String propertyFileName = this.getInitParameter("myConf");
			prop.load(getClass().getClassLoader().getResourceAsStream(propertyFileName));
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			dbConn = ds.getConnection();
			
			//out.println("Conn: "+dbConn.toString());
			Statement st = dbConn.createStatement();
			ResultSet rs = st.executeQuery("SELECT NUMQ,NUMQSEC,QUERY FROM PALMOBJPGM.NPR_RQUERY WHERE REPCOD='"+sReporte+"' ORDER BY NUMQ,NUMQSEC");
			
			while ( rs.next() )
			{
				
			}
			
			
			
		}catch( Exception ex )
		{
			ex.printStackTrace();
		}
		
		


		out.println(this.getFooterBS());
		out.close();
	}
}
