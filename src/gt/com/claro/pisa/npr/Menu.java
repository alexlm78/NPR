package gt.com.claro.pisa.npr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import gt.com.claro.pisa.npr.ugp.Users;

/**
 * Menu generation from DB.
 * 
 * @author Alejandro Lopez Monzon
 * @version 1.1
 * @category sys.
 */
public class Menu extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet 
{
	private static final long serialVersionUID = 4423171965511165172L;
	private Connection connection= null;
	protected String Schema = "PALMOBJPGM";
	protected String User="";
	protected Properties Props = null;
	protected PrintWriter out;
	protected Integer slvl = 0;
	Users user;
	String UName;

	/**
	 * Class Contructor
	 * 
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public Menu() 
	{
		super();
		try
		{
			Props = new Properties();
			String propertyFileName = "conf.properties";
			Props.load(getClass().getClassLoader().getResourceAsStream(propertyFileName));
			Context ctx = new InitialContext();
			//DataSource ds = (DataSource) ctx.lookup("jdbc/GUA870");
			Schema = Props.getProperty("schema")!=null ? Props.getProperty("schema").trim() : this.Schema;
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			connection = ds.getConnection();
		}catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Initialization process
	 * 
	 * @see javax.servlet.http.HttpServlet#init()
	 */
	public void init() throws ServletException
	{
		super.init();
		try
		{
			Props = new Properties();			
			String propertyFileName = this.getServletConfig().getInitParameter("myConf");
			Props.load(getClass().getClassLoader().getResourceAsStream(propertyFileName));//*/
			Context ctx = new InitialContext();
			//DataSource ds = (DataSource) ctx.lookup("jdbc/GUA870");
			Schema = Props.getProperty("schema")!=null ? Props.getProperty("schema").trim() : this.Schema;
			DataSource ds = (DataSource) ctx.lookup(Props.getProperty("jndi"));
			connection = ds.getConnection();			
		}catch( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{		
		response.setHeader("Cache-Control", "no-Cache");
		response.setHeader("Pragma", "No-cache");
		this.User = (request.getRemoteUser()!=null ? request.getRemoteUser() : Props.getProperty("user"));
		user = new Users(Props, User);
		UName = user.getLocalNombre();
		response.setContentType("text/html");
		out = response.getWriter();
		if( getUserSecLvl(User)>= slvl )
			out.println(this.setMenuBS(this.getUserMenu(this.User)));
		else
			out.println(this.setAuthorizationReq());
	}  	
	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setHeader("Cache-Control", "no-Cache");
		response.setHeader("Pragma", "No-cache");
		this.User = (request.getRemoteUser()!=null ? request.getRemoteUser() : this.User);
		response.setContentType("text/html");
		out = response.getWriter();
		out.println(this.setMenuBS(this.getUserMenu(this.User)));
	}
	
	/**
	 * Make the page header with menu.
	 * 
	 * @return String with the generated header.
	 */
	public String setMenuBS(String Menu)
	{
		String sMenu = "";
		sMenu += "<!DOCTYPE html>\n";
		sMenu += "<html>\n";
		sMenu += "<head>\n";
		sMenu += "	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n";
		sMenu += "	<title>New Pisa Reports</title>\n";
		sMenu += "	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n";
		sMenu += "	<link rel=\"shortcut icon\" href=\"images/favicon.ico\">\n";
		sMenu += "	<link href=\"css/bootstrap.css\" rel=\"stylesheet\" media=\"screen\">\n";
		sMenu += "  <link href=\"css/font-awesome.css\" rel=\"stylesheet\">\n";
		sMenu += "  <link href=\"css/daterangepicker-bs3.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\n";
		sMenu += "	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n";
		sMenu += "	<!--[if lt IE 9]>\n";
		sMenu += "		<script src=\"js/html5shiv.js\"></script>\n";
		sMenu += "		<script src=\"js/respond.min.js\"></script>\n";
		sMenu += "	<![endif]-->\n";
		sMenu += "	<link href=\"css/kreaker.css\" rel=\"stylesheet\">\n";
		sMenu += "</head>\n";
		sMenu += "<body>\n";
		sMenu += "	<div class=\"navbar navbar-default navbar-fixed-top\"><!-- Fixed navbar -->\n";
		sMenu += "		<div class=\"container\">\n";
		sMenu += "			<div class=\"navbar-header\">\n";
		sMenu += "				<a class=\"navbar-brand\" href=\"Main\" title=\"New Pisa Reports\">NPR</a>\n";
		sMenu += "			</div>\n";
		sMenu += "			<div class=\"navbar-collapse collapse\">\n";
		sMenu += "				<ul class=\"nav navbar-nav\">\n";
		sMenu += this.getMenuTreeBS(this.getUserMenu(User),true);
		sMenu += "					<li id=\"fat-menu\">\n";
		sMenu += "						<a href=\"Logout\" id=\"logout\" role=\"button\">Salir</a>\n";
		sMenu += "					</li>\n";
		sMenu += "				</ul>\n";
		sMenu += "				<ul class=\"nav navbar-nav navbar-right\">\n";
		sMenu += "                   <li><a href=\"#\"><span class=\"label label-primary\" title=\""+user.getLocalNombre()+"\">"+User+"</span></a></li>\n";
		sMenu += "				</ul>\n";
		sMenu += "			</div>\n";
		sMenu += "		</div>\n";
		sMenu += "	</div> <!-- Fixed navbar End-->\n";
		
		return sMenu;
	}
	
	/**
	 * Get the menu from the group for a user.
	 * 
	 * @param user User from who get the menu.
	 * @return Menu name for a user.
	 */
	public String getUserMenu( String user )
	{
		String Resp = "";
		String iUser = user.toUpperCase();
		String q = 	"SELECT G.MENUINI "+
					"  FROM "+Schema+".A_USERS U "+
					" INNER JOIN "+Schema+".A_GROUPS G ON ( U.IDGRUPO=G.IDGRUPO ) "+
					" WHERE LOGIN='"+iUser+"'";
		
		try
		{			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(q);
			if( rs.next() )
			{
				Resp = rs.getString(1).trim();
			}
			rs.close();			
		}catch( Exception ex )
		{
			ex.printStackTrace();
		}
		
		return Resp;
	}
	
	/**
	 * Get the security level for a user
	 * 
	 * @param user User to get the sec level
	 * @return The security level for <i>user</i> 
	 */
	public Integer getUserSecLvl( String user )
	{
		Integer Resp=-99;
		String iUser = user.toUpperCase();
		String q = 	"SELECT U.STSSEG "+
					"  FROM PALMOBJPGM.A_USERS U "+
					" WHERE U.LOGIN='"+iUser+"'";
		//System.out.println(q);
		try
		{			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(q);
			if( rs.next() )
				Resp = Integer.valueOf(rs.getInt(1));
			else
				Resp = -1;
			
			rs.close();			
		}catch( Exception ex )
		{
			ex.printStackTrace();
		}
		
		System.out.println(user.trim()+":"+Resp.toString());
		return Resp;
	}
	
	/**
	 * Get the security level for an app
	 * 
	 * @param app App name to get the securty level
	 * @return The security level for <i>app</i> 
	 */
	public Integer getAppSecLvl( String app )
	{
		Integer Resp = -1;
		String iApp = app.toUpperCase();
		String q = 	"SELECT O.STSSEG "+
					"  FROM "+Schema+".A_OPTIONS O "+
					" WHERE O.OPCION='"+iApp+"'";
		
		try
		{			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(q);
			if( rs.next() )
				Resp = rs.getInt(1);
			
			rs.close();			
		}catch( Exception ex )
		{
			ex.printStackTrace();
			Resp = -1;
		}
		
		return Resp;
	}
	
	/**
	 * Get the menu dropdown from db for applicaciont menu.
	 * 
	 * @param ini Initial menu.
	 * @param dropdown menu or submenus flag.
	 * @return HTML Generated.
	 */
	public String getMenuTreeBS( String ini, boolean dropdown )
	{
		String Resp = "";
		String iMenu = ini.toUpperCase();
		//String q = "select m.opcion, o.descripcion, o.tipo, o.path from dby_menus m inner join dby_opciones o on ( o.opcion=m.opcion ) where m.menu = '"+iMenu+"'";
		String q = 	"SELECT M.OPCION, O.DESCRIP, O.TIPO, O.URL "+
					"  FROM "+Schema+".A_MENUS M "+
					" INNER JOIN "+Schema+".A_OPTIONS O ON ( O.OPCION=M.OPCION ) "+
					" WHERE M.MENU = '"+iMenu+"' ORDER BY M.ORDEN ";
		
		try
		{			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(q);
			while( rs.next() )
			{
				if ( rs.getString(3).compareToIgnoreCase("M")==0 )
				{
					if ( dropdown )
					{	
						Resp += "<li class=\"dropdown\">\n";
						Resp += "<a id=\""+rs.getString(1).trim()+"\" href=\"#\" role=\"button\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+rs.getString(2).trim()+"<b class=\"caret\"></b></a>\n";
						Resp += "<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\""+rs.getString(2).trim()+"\">\n";
						Resp += this.getMenuTreeBS(rs.getString(1).trim(),false);
	                  	Resp += " </ul></li>\n";
					}else
					{
						Resp += "<li class=\"dropdown-submenu\">";
						Resp += "<a tabindex=\"-1\" href=\"#\">"+rs.getString(2).trim()+"</a>\n";
						Resp += "<ul class=\"dropdown-menu\">\n";
						Resp += this.getMenuTreeBS(rs.getString(1).trim(),false);
						Resp += "</ul></li>\n";
					}
				}else
					Resp += "<li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\""+rs.getString(4).trim()+"\">"+rs.getString(2).trim()+"</a></li>\n";
			}
			rs.close();
		}catch( Exception ex )
		{
			ex.printStackTrace();
		}
		
		return Resp;
	}
	
	/**
	 * Set the <i></b>No authorization access</b></i> warning.
	 * 
	 * @return HTML code generated.
	 */
	public String setAuthorizationReq()
	{
		String sMenu="";
		sMenu += "<!DOCTYPE html>\n";
		sMenu += "<html>\n";
		sMenu += "<head>\n";
		sMenu += "	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n";
		sMenu += "	<title>Pisa Reports Admin</title>\n";
		sMenu += "	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n";
		sMenu += "	<link rel=\"shortcut icon\" href=\"images/favicon.ico\">\n";
		sMenu += "	<link href=\"css/bootstrap.css\" rel=\"stylesheet\" media=\"screen\">\n";
		sMenu += "	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n";
		sMenu += "	<!--[if lt IE 9]>\n";
		sMenu += "		<script src=\"js/html5shiv.js\"></script>\n";
		sMenu += "		<script src=\"js/respond.min.js\"></script>\n";
		sMenu += "	<![endif]-->\n";		
		sMenu += "	<link href=\"css/kreaker.css\" rel=\"stylesheet\">\n";
		sMenu += "</head>\n";		
		sMenu += "<body>\n";
		sMenu += "	<div class=\"container\">\n";
		sMenu += "		<div class=\"alert alert-error\">\n";
		sMenu += "			<h2>Error!</h2>\n";
		sMenu += "			<h4>El usuario "+User+" no tiene acceso a esta aplicacion</h4>\n";
		sMenu += "		</div>\n";
		sMenu += "	</div>\n";
				
		return sMenu;
	}
	
	/**
	 * Get the page footer.
	 * 
	 * @return HTML Generated.
	 */
	public String getFooterBS()
	{
		String sFooter = "";
		
		sFooter += "<!-- Footer -->\n";
		sFooter += "<footer class=\"footer\">\n";
		sFooter += "	<div class=\"col-md-12 text-left\"><span class=\"label label-default\">Copyright &copy; 2013 - 2015 Alejandro Lopez Monzon</span></div>\n";
		sFooter += "</footer>\n";
		sFooter += "<!-- Script loading... -->\n";
		sFooter += "<script src=\"js/jquery.js\"></script>\n";
		//sFooter += "<script src=\"js/bootstrap.js\"></script>\n";
		sFooter += "<script src=\"js/dropdown.js\"></script>\n";
		sFooter += "<script src=\"js/kreaker.js\"></script>\n";
		sFooter += "<script src=\"js/moment.js\"></script>\n";
		sFooter += "<script src=\"js/daterangepicker.js\"></script>\n";
		
		return sFooter;
	}
}